from flask import render_template, redirect, url_for, request, flash
from app import app
from flask import Blueprint
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_required, current_user
from models import *
from sqlalchemy.sql.expression import func
import datetime

views = Blueprint('views', __name__)


@app.route("/")
def home():
    albums = Album.query.order_by(Album.dateDeSortie.desc())
    artistes = Artist.query.order_by(func.random())
    return render_template(
        "accueilNonConnect.html",
        title="Hitflix",
        artistes=artistes,
        albums=albums,
        funcArtist=get_artist_by_id
    )


@app.route("/connected/")
@login_required
def homeConnected():
    albums = Album.query.order_by(Album.dateDeSortie.desc()).limit(14).all()
    artistes = Artist.query.order_by(func.random()).limit(18).all()
    return render_template(
        "accueilConnecte.html",
        title="Home",
        pseudo=current_user.pseudo,
        artistes=artistes,
        albums=albums,
        funcArtist=get_artist_by_id
    )


@app.route("/connected/<string:selection>/")
@login_required
def selection(selection):
    if selection not in ['album', 'playlist', 'artiste', 'recherche']:
        return redirect(url_for('home'))

    if selection == 'recherche':
        args = request.args.get("args")
        recherche = False
        if args:
            recherche = True
            args = "%{}%".format(args)
            tracks = Chanson.query.filter(
                Chanson.titre.like(args)).limit(10).all()
            albums = Album.query.filter(Album.titre.like(args)).limit(10).all()
            artistes = Artist.query.filter(
                Artist.nom.like(args)).limit(10).all()
            return render_template(
                "recherche.html",
                title="Hitflix",
                recherche=recherche,
                pseudo=current_user.pseudo,
                tracks=tracks,
                albums=albums,
                artistes=artistes,
                len=len,
                funcArtist=get_artist_by_id
            )
        return render_template(
            "recherche.html",
            title="Hitflix",
            recherche=recherche,
            pseudo=current_user.pseudo
        )

    args = request.args.get("args")
    recherche = False
    if args:
        recherche = True
        args = "%{}%".format(args)
        playlists = db.session.query(Playlist).filter(
            Playlist.nom.like(args), Playlist.utilisateur_id == current_user.id).all()
        albums = db.session.query(Album).join(aime).filter(
            Album.titre.like(args), aime.id_utilisateur == current_user.id).all()
        artistes = db.session.query(Artist).join(suivre).filter(
            Artist.nom.like(args), suivre.id_utilisateur == current_user.id).all()
        return render_template(
            "connected.html",
            title="Hitflix",
            selection=selection,
            pseudo=current_user.pseudo,
            playlists=playlists,
            artistes=artistes,
            albums=albums,
            funcArtist=get_artist_by_id,
            recherche=recherche,
        )
    return render_template(
        "connected.html",
        title="Hitflix",
        selection=selection,
        pseudo=current_user.pseudo,
        playlists=get_playlists_by_user(current_user.id),
        artistes=get_artistsFollowed_by_user(current_user.id),
        albums=get_albumsLiked_by_user(current_user.id),
        funcArtist=get_artist_by_id,
        recherche=recherche
    )


@app.route("/connected/profil/")
@login_required
def profil():
    return render_template(
        "profil.html",
        title="Profil",
        nom=current_user.nom,
        prenom=current_user.prenom,
        pseudo=current_user.pseudo,
        dateNaiss=current_user.dateNaiss,
        sexe=current_user.sexe,
        dateInscription=current_user.dateInscription,
        urlImg=current_user.urlImage,
    )


@app.route("/connected/profil/modif")
@login_required
def modifProfil():
    return render_template(
        "modifProfil.html",
        title="Profil",
        nom=current_user.nom,
        prenom=current_user.prenom,
        pseudo=current_user.pseudo,
        dateNaiss=current_user.dateNaiss,
        sexe=current_user.sexe,
        mdp=current_user.mdp,
        dateInscription=current_user.dateInscription,
        urlImg=current_user.urlImage,
    )


@app.route("/connected/artiste/<int:id>/")
@login_required
def artiste(id):
    artiste = get_artist_by_id(id)
    albums = get_album_by_artist(id)
    followed = False
    artistsFollowed = get_artistsFollowed_by_user(current_user.id)
    for a in artistsFollowed:
        if a.id == id:
            followed = True
    return render_template(
        "artiste.html",
        title=artiste.nom,
        pseudo=current_user.pseudo,
        artiste=artiste,
        albums=albums,
        followed=followed,
        funcArtist=get_artist_by_id
    )


@app.route("/connected/album/<int:id>/")
@login_required
def album(id):
    album = get_album_by_id(id)
    artiste = get_artist_by_id(album.auteur_id)
    albumContenu = get_tracks_from_album(album.id)
    liked = False
    id_user = current_user.id
    artistsLiked = get_albumsLiked_by_user(id_user)
    note = get_note_album(id)
    note_effectuer = get_deja_note(id_user,id)
    deja_note = False
    if  note_effectuer:
        deja_note = True
    for a in artistsLiked:
        if a.id == id:
            liked = True
    dictPlaylists = {}
    tempPlaylists = get_playlists_by_user(id_user)
    for p in tempPlaylists:
        tempMusic = get_tracks_by_playlist(p.id)
        dictPlaylists[p.nom] = []
        for m in tempMusic:
            dictPlaylists[p.nom].append(m.idChanson)
    return render_template(
        "album.html",
        title=album.titre,
        pseudo=current_user.pseudo,
        album=album,
        artiste_nom=artiste.nom,
        albumContenu=albumContenu,
        liked=liked,
        dictPlaylists = dictPlaylists,
        note = note,
        deja_note = deja_note,
        notePerso = note_effectuer,
        round = round
    )


@app.route("/connected/playlist/<string:name>/")
@login_required
def playlist(name):
    playlist = get_playlist_by_name(current_user.id, name)
    playlistContenu = get_tracks_from_playlist(playlist.id)
    tempsTotal = datetime.timedelta(minutes=0, seconds=0)
    for track in playlistContenu:
        if track.duree != None:
            tempsTotal += datetime.timedelta(minutes=track.duree.minute,
                                            seconds=track.duree.second)
    return render_template(
        "playlist.html",
        title=name,
        pseudo=current_user.pseudo,
        playlist=playlist,
        nbTitres=len(playlistContenu),
        dureeTotal=tempsTotal,
        playlistContenu=playlistContenu,
        funcArtist=get_artist_by_id,
        funcAlbum=get_album_by_id
    )


@app.route("/connected/playlist/create", methods=["POST"])
@login_required
def playlist_post():
    id = current_user.id
    nom = request.form.get("nom")
    playlist = get_playlist_by_name(id, nom)
    if playlist:
        flash("Ce nom est déjà utilisé.")
        return redirect("/connected/playlist")
    image = request.form.get("image")
    if not image:
        image = url_for('static', filename='img/music-album.svg')
    new_playlist = Playlist(
        nom=nom, urlImage=image, dateCreation=datetime.date.today(), utilisateur_id=id)
    db.session.add(new_playlist)
    db.session.commit()
    return redirect("/connected/playlist/"+nom+"/")


@app.route("/connected/playlist/<string:name>/modif", methods=['POST'])
def modifPlaylist_post(name):
    id = current_user.id
    playlist = Playlist.query.filter_by(nom=name).first()
    titre = request.form.get('nom')
    tempPlaylist = get_playlist_by_name(id, titre)
    if tempPlaylist:
        if tempPlaylist.nom != name:
            flash("Ce nom est déjà utilisé.")
            return redirect("/connected/playlist/"+name)
    playlist.nom = titre
    image = request.form.get("image")
    if not image:
        image = url_for('static', filename='img/music-album.svg')
    playlist.urlImage = image
    db.session.commit()
    return redirect("/connected/playlist/"+titre)


@app.route("/connected/artiste/<int:id>/follow")
def followArtist(id):
    newFollow = suivre(id_artist=id, id_utilisateur=current_user.id)
    db.session.add(newFollow)
    db.session.commit()
    return redirect("/connected/artiste/"+str(id)+"/")


@app.route("/connected/album/<int:id>/like")
def likeAlbum(id):
    newLike = aime(id_album=id, id_utilisateur=current_user.id)
    db.session.add(newLike)
    db.session.commit()
    return redirect("/connected/album/"+str(id)+"/")


@app.route("/connected/artiste/<int:id>/unfollow")
def unfollowArtist(id):
    db.session.query(suivre).filter(suivre.id_artist == id,
                                    suivre.id_utilisateur == current_user.id).delete()
    db.session.commit()
    return redirect("/connected/artiste/"+str(id)+"/")


@app.route("/connected/album/<int:id>/unlike")
def unlikeAlbum(id):
    db.session.query(aime).filter(aime.id_album == id,
                                  aime.id_utilisateur == current_user.id).delete()
    db.session.commit()
    return redirect("/connected/album/"+str(id)+"/")


@app.route("/connected/playlist/<string:name>/delete")
@login_required
def playlistDelete(name):
    id_user = current_user.id
    playlist = get_playlist_by_name(id_user, name)
    db.session.query(contenir).filter(
        contenir.idPlaylist == playlist.id).delete()
    db.session.delete(playlist)
    db.session.commit()
    return redirect("/connected/playlist")


@app.route("/connected/playlist/<string:name>/add/<int:id>/<int:idAlbum>")
@login_required
def playlistAddTrack(name, id, idAlbum):
    id_user = current_user.id
    playlist = get_playlist_by_name(id_user, name)
    contenu = contenir(idChanson=id, idPlaylist=playlist.id)
    db.session.add(contenu)
    db.session.commit()
    return redirect("/connected/album/"+str(idAlbum))

@app.route("/connected/playlist/<string:name>/remove/<int:id>")
@login_required
def playlistRemoveTrack(name, id):
    id_user = current_user.id
    playlist = get_playlist_by_name(id_user, name)
    contenu = contenir.query.filter_by(idChanson=id,idPlaylist=playlist.id).first()
    db.session.delete(contenu)
    db.session.commit()
    album = Chanson.query.filter_by(id=id).first()
    return redirect("/connected/playlist/"+name)

@app.route("/connected/album/<int:id>/noter", methods=['POST'])
@login_required
def ajoutNote(id):
    id_user = current_user.id
    val = request.form.get("val")
    new_note = note(id_note = get_new_id_note(), id_utilisateur = id_user, id_album = id, note_album= val)
    db.session.add(new_note)
    db.session.commit()
    return redirect("/connected/album/"+str(id))

@app.route("/connected/album/<int:id>/note/modif", methods=['POST'])
@login_required
def modifNote(id):
    id_user = current_user.id
    note = get_deja_note(id_user,id)
    val = request.form.get('val')
    note.note_album = val
    db.session.commit()
    return redirect("/connected/album/"+str(id))

