from app import db
from flask_login import UserMixin


class Utilisateur(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pseudo = db.Column(db.String(50), primary_key=True)
    prenom = db.Column(db.String(50))
    nom = db.Column(db.String(50))
    urlImage = db.Column(db.String(300))
    dateNaiss = db.Column(db.Date)
    sexe = db.Column(db.String(50))
    mdp = db.Column(db.String(150))
    dateInscription = db.Column(db.Date)

    def get_pseudo(self):
        return self.pseudo


class Artist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(50))

    def get_nom(self):
        return self.nom

    def __repr__(self):
        return self.nom


class Chanson(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(200))
    auteur_id = db.Column(db.Integer, db.ForeignKey("artist.id"))
    album_id = db.Column(db.Integer, db.ForeignKey("album.id"))
    duree = db.Column(db.Time)
    urlImage = db.Column(db.String(300))

    def __repr__(self):
        return "%s de %s" % (self.titre, self.auteur_nom)


class Album(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(200))
    auteur_id = db.Column(db.String(50), db.ForeignKey("artist.nom"))
    dateDeSortie = db.Column(db.Date)
    urlImage = db.Column(db.String(300))

    def __repr__(self):
        return "%s de %s" % (self.titre, self.auteur_nom)


class Playlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(200))
    urlImage = db.Column(db.String(300))
    dateCreation = db.Column(db.Date)
    utilisateur_id = db.Column(db.Integer, db.ForeignKey("utilisateur.id"))

    def __repr__(self):
        return "Playlist %s de %s" % (self.nom, self.dateCreation)


class suivre(db.Model):
    id_artist = db.Column(db.Integer, db.ForeignKey(
        "artist.id"), primary_key=True)
    id_utilisateur = db.Column(db.Integer, db.ForeignKey(
        "utilisateur.id"), primary_key=True)


class aime(db.Model):
    id_album = db.Column(db.Integer, db.ForeignKey(
        "album.id"), primary_key=True)
    id_utilisateur = db.Column(db.Integer, db.ForeignKey(
        "utilisateur.id"), primary_key=True)


class contenir(db.Model):
    idChanson = db.Column(db.Integer, db.ForeignKey(
        "chanson.id"), primary_key=True)
    idPlaylist = db.Column(db.Integer, db.ForeignKey(
        "playlist.id"), primary_key=True)

class note(db.Model):
    id_note = db.Column(db.Integer, primary_key = True)
    id_utilisateur = db.Column(db.Integer, db.ForeignKey(
        "utilisateur.id"))
    id_album = db.Column(db.Integer, db.ForeignKey(
        "album.id"))
    note_album = db.Column(db.Float(1,1))
    


def find_pseudo(pseudo):
    return Utilisateur.query.filter_by(pseudo=pseudo).first()


def find_by_id(id):
    return Utilisateur.query.filter_by(id=id).first()


def get_new_id():
    return len(Utilisateur.query.all())+1


def get_album_by_id(id):
    return Album.query.filter_by(id=id).first()


def get_artist_by_id(id):
    return Artist.query.filter_by(id=id).first()


def get_album_by_artist(id):
    return Album.query.filter_by(auteur_id=id).all()


def get_tracks_from_album(id):
    return Chanson.query.filter_by(album_id=id).all()


def get_playlists_by_user(id):
    return Playlist.query.filter_by(utilisateur_id=id).all()


def get_playlist_by_name(id, nom):
    return Playlist.query.filter_by(nom=nom, utilisateur_id=id).first()


def get_tracks_from_playlist(id):
    return db.session.query(Chanson).join(contenir).filter(contenir.idPlaylist == id).all()


def get_artistsFollowed_by_user(id):
    return db.session.query(Artist).join(suivre).filter(suivre.id_utilisateur == id).all()


def get_albumsLiked_by_user(id):
    return db.session.query(Album).join(aime).filter(aime.id_utilisateur == id).all()

def get_tracks_by_playlist(id):
    return contenir.query.filter_by(idPlaylist=id).all()

def get_note_album(id):
    notes = note.query.filter_by(id_album=id).all()
    if len(notes) == 0:
        return "Pas de note"
    res = 0
    for elem in notes:
        res += elem.note_album
    return round(res/len(notes),2)

def get_deja_note(id_user,id_album):
    return note.query.filter_by(id_utilisateur=id_user, id_album=id_album).first()

def get_new_id_note():
    return len(note.query.all())+1
