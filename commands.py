import click
import json
import datetime
from app import app, db
from models import Utilisateur, Artist, Album, Chanson
from werkzeug.security import generate_password_hash, check_password_hash

@app.cli.command()
def loaddb():
    """
    Creates the tables and populates them with data.
    """
    # création de toutes les tables
    db.create_all()

    # chargement de notre jeu de données
    with open("data/albums_data.json", "r") as read_file:
        albums = json.load(read_file)
    with open("data/tracks_data.json", "r") as read_file:
        tracks = json.load(read_file)
    
    # import des modèles
    # première passe: création de tous les artistes
    artists = {}
    for a in albums:
        if albums[a][0]["artist_id"] not in artists:
            newArtist = Artist(id=albums[a][0]["artist_id"],nom=albums[a][0]["artist_name"])
            db.session.add(newArtist)
            artists[albums[a][0]["artist_id"]] = newArtist
    db.session.commit()

    # deuxième passe: création de tous les albums
    for a in albums:
        art = artists[albums[a][0]["artist_id"]]
        extractDate = int(albums[a][0]["album_release"])
        if extractDate != 0:
            date = datetime.date(year=extractDate,month=1,day=1)
            newAlbum = Album(id=albums[a][0]["album_id"],titre=a,auteur_id=art.id,dateDeSortie=date,urlImage=albums[a][0]["album_cover"])
        else:
            newAlbum = Album(id=albums[a][0]["album_id"],titre=a,auteur_id=art.id,dateDeSortie=None,urlImage=albums[a][0]["album_cover"])
        db.session.add(newAlbum)
    db.session.commit()

    # troisième passe: création de tous les tracks
    for t in range(len(tracks)):
        art = artists[tracks[t]["track_id_artist"]]
        temp = int(tracks[t]["track_duration"])
        temp = temp // 1000
        minute = temp // 60
        seconde = temp % 60
        if minute > 0 and minute <= 59 and seconde > 0 and seconde <= 59:
            temp = datetime.time(minute=minute,second=seconde)
            newTrack = Chanson(id=tracks[t]["track_id"],titre=tracks[t]["track_name"],auteur_id=art.id,album_id=tracks[t]["track_album_id"],duree=temp,urlImage=tracks[t]["track_img"])
        else:
            newTrack = Chanson(id=tracks[t]["track_id"],titre=tracks[t]["track_name"],auteur_id=art.id,album_id=tracks[t]["track_album_id"],duree=None,urlImage=tracks[t]["track_img"])
        db.session.add(newTrack)
    db.session.commit()

@app.cli.command()
def syncdb():
    '''Creates all missing tables.'''
    db.create_all()