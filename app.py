from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
import os.path
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__,static_url_path='/static')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)

def mkpath(p):
    return os.path.normpath (
        os.path.join(
        os.path.dirname(__file__ ),
        p))

app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('./hitflix.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

app.config["SECRET_KEY"]="hitflix"

from auth import auth as auth_blueprint
app.register_blueprint(auth_blueprint)
from views import views as views_blueprint
app.register_blueprint(views_blueprint)

import commands

login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.init_app(app)
from models import Utilisateur, find_by_id
@login_manager.user_loader
def load_user(id):
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return find_by_id(id)