from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from app import app, db
from models import Utilisateur , find_pseudo, get_new_id, find_by_id
from flask_login import login_user, login_required, logout_user, current_user
import datetime
auth = Blueprint('auth', __name__)

@auth.route("/login/")
def login():
    return render_template(
        "connexion.html",
        title = "Login"
    )

@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("home"))

@auth.route("/login/", methods=['POST'])
def login_post():
    pseudo = request.form.get('pseudo')
    password = request.form.get('mdp')
    user = find_pseudo(pseudo)
    # check if the user actually exists
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.mdp, password):
        flash('Vérifiez vos identifiants et réessayez')
        return redirect(url_for('auth.login')) # if the user doesn't exist or password is wrong, reload the page
    # if the above check passes, then we know the user has the right credentials
    login_user(user)
    return redirect(url_for("homeConnected"))


@auth.route("/register/")
def register():
    return render_template(
        "register.html",
        title = "Register"
    )

@auth.route("/register", methods=['POST'])
def register_post():
    pseudo = request.form.get('pseudo')
    user = find_pseudo(pseudo)
    if user:
        flash("Votre pseudo est déjà utilisé.")
        return redirect(url_for("auth.register"))

    mdp = request.form.get('mdp')
    id = get_new_id()
    prenom = request.form.get('prenom')
    nom = request.form.get('nom')
    extractDate = request.form.get('dateNaissance')
    annee = int(extractDate[0:4])
    mois = int(extractDate[5:7])
    jour = int(extractDate[8:10])
    dateNaissance = datetime.date(annee,mois,jour)
    sexe = request.form.get('sexe')
    urlImg = request.form.get('urlImg')
    dateInscription = datetime.date.today()
    new_user = Utilisateur(id = id,pseudo = pseudo, prenom=prenom, nom=nom, urlImage=urlImg, dateNaiss=dateNaissance, sexe=sexe, mdp=generate_password_hash(mdp, method='sha256'), dateInscription = dateInscription)
    db.session.add(new_user)
    db.session.commit()
    return redirect(url_for("auth.login"))

@auth.route("/connected/profil/modif/")
def modifProfil():
    return render_template(
        "modifProfil.html",
        title = "Profil",
        nom = current_user.nom,
        prenom = current_user.prenom,
        pseudo = current_user.pseudo,
        dateNaiss = current_user.dateNaiss,
        sexe = current_user.sexe,
        mdp = current_user.mdp,
        dateInscription = current_user.dateInscription,
        urlImg = current_user.urlImage,
    )

@auth.route("/connected/profil/modif", methods=['POST'])
def modifProfil_post():
    user = find_by_id(current_user.id)
    pseudo = request.form.get('pseudo')
    tempUser = find_pseudo(pseudo)
    if tempUser:
        if tempUser.pseudo != current_user.pseudo:
            flash("Votre pseudo est déjà utilisé.")
            return redirect(url_for("auth.modifProfil"))

    user.pseudo = pseudo
    if request.form.get('mdp'):
        user.mdp = generate_password_hash(request.form.get('mdp'),method='sha256')
    user.prenom = request.form.get('prenom')
    user.nom = request.form.get('nom')
    extractDate = request.form.get('dateNaissance')
    annee = int(extractDate[0:4])
    mois = int(extractDate[5:7])
    jour = int(extractDate[8:10])
    user.dateNaissance = datetime.date(annee,mois,jour)
    user.sexe = request.form.get('sexe')
    user.urlImage = request.form.get('urlImg')
    db.session.commit()
    return redirect(url_for("profil"))